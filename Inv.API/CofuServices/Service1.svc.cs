﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Net.Mail;

namespace CofuServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        //INVENTARIOS
        /*[OperationContract]
        [WebGet(UriTemplate = "/sesion/?username={username}&password={password}", ResponseFormat = WebMessageFormat.Json)]
        Status getLogin(string username, string password);

        [OperationContract]
        [WebGet(UriTemplate = "/detallebien/?numinv={numinv}", ResponseFormat = WebMessageFormat.Json)]
        bienID GetBienID(string numinv);

        [OperationContract]
        [WebGet(UriTemplate = "/detallebienCAP/?numinv={numinv}", ResponseFormat = WebMessageFormat.Json)]
        bienID GetBienIDCAP(string numinv);

        [OperationContract]
        [WebGet(UriTemplate = "detallebienNuevo/?numinv={numinv}", ResponseFormat = WebMessageFormat.Json)]
        bienID GetBienIDNUEVO(string numinv);

        [OperationContract]
        [WebGet(UriTemplate = "/obtenerbienes", ResponseFormat = WebMessageFormat.Json)]
        Bienesm GetBienesm();

        [OperationContract]
        [WebGet(UriTemplate = "/obtenerbienesCAP", ResponseFormat = WebMessageFormat.Json)]
        Bienesm GetBienesmCAP();

        [OperationContract]
        [WebGet(UriTemplate = "/agregarbien/?IDuser={IDuser}&numinv={numinv}&numinvnuevo={numinvnuevo}&marca={marca}&modelo={modelo}&serie={serie}&nofactura={nofactura}&caracteristicas={caracteristicas}&observaciones={observaciones}&capitalizable={capitalizable}&EdificioID={EdificioID}&TipoActivo={TipoActivo}", ResponseFormat = WebMessageFormat.Json)]
        Status setBien(int IDuser, string numinv, string numinvnuevo, string marca, string modelo, string serie, int nofactura, string caracteristicas, string observaciones, int Capitalizable, int EdificioID, int TipoActivo);

        [OperationContract]
        [WebGet(UriTemplate = "/editarbien/?IDbien={IDbien}&numinv={numinv}&numinvnuevo={numinvnuevo}&marca={marca}&modelo={modelo}&serie={serie}&nofactura={nofactura}&caracteristicas={caracteristicas}&observaciones={observaciones}&capitalizable={capitalizable}&EdificioID={EdificioID}&TipoActivo={TipoActivo}", ResponseFormat = WebMessageFormat.Json)]
        Status UpdateBien(int IDbien, string numinv, string numinvnuevo, string marca, string modelo, string serie, int nofactura, string caracteristicas, string observaciones, int Capitalizable, int EdificioID, int TipoActivo);

        [OperationContract]
        [WebGet(UriTemplate = "/borrarbien/?IDbien={IDbien}", ResponseFormat = WebMessageFormat.Json)]
        Status DeleteBien(int IDbien);

        [OperationContract]
        [WebGet(UriTemplate = "/obteneredificios", ResponseFormat = WebMessageFormat.Json)]
        Edificiom GetEdificiom();

        [OperationContract]
        [WebGet(UriTemplate = "/obtenerbienres/?noemp={noemp}", ResponseFormat = WebMessageFormat.Json)]
        bienresguardom GetBienresguardom(string noemp);

        [OperationContract]
        [WebGet(UriTemplate = "/obtenerinventarioF/?noemp={noemp}", ResponseFormat = WebMessageFormat.Json)]
        bienresguardom GetInventarioFisico(string noemp);

        [OperationContract]
        [WebGet(UriTemplate = "/agregarinventario/?noempleado={noempleado}&idbien={idbien}&idresguardo={idresguardo}&isnew={isnew}&noinv={noinv}&iscap={iscap}&noinvnuevo={noinvnuevo}&EdificioID={EdificioID}&idusuario={idusuario}", ResponseFormat = WebMessageFormat.Json)]
        Status setinventario(int noempleado, int idbien, int idresguardo, int isnew, string noinv, int iscap, string noinvnuevo, int EdificioID, int idusuario);

        [OperationContract]
        [WebGet(UriTemplate = "Obtenerempledo/?noempleado={noempleado}", ResponseFormat = WebMessageFormat.Json)]
        empleado getEmpleado(int noempleado);

        [OperationContract]
        [WebGet(UriTemplate = "CambiarEstado/?IdResgFisico={IdResgFisico}", ResponseFormat = WebMessageFormat.Json)]
        Status SetEstadoFisico(int IdResgFisico);

        [OperationContract]
        [WebGet(UriTemplate = "BorrarInventario/?IdResgFisico={IdResgFisico}", ResponseFormat = WebMessageFormat.Json)]
        Status BorrarInventarioFisico(int IdResgFisico);

        [OperationContract]
        [WebGet(UriTemplate = "SetInvTransito/?IDEdificio={IDEdificio}&IDUsuario={IDUsuario}&nombreEntrega={nombreEntrega}&nombreRecibe={nombreRecibe}&noInv={noInv}&horaSalida={horaSalida}&Observaciones={Observaciones}", ResponseFormat = WebMessageFormat.Json)]
        Status setEnviarInventario(int IDEdificio, int IDUsuario, string nombreEntrega, string nombreRecibe, string noInv, string horaSalida, string Observaciones);

        [OperationContract]
        [WebGet(UriTemplate = "ObtenerTransito/?IDEdificio={IDEdificio}", ResponseFormat = WebMessageFormat.Json)]
        ListaTransito GetTransito(int IDEdificio);

        [OperationContract]
        [WebGet(UriTemplate = "BorrarTransito/?idarticulo={idarticulo}", ResponseFormat = WebMessageFormat.Json)]
        Status DeleteArticulo(int idarticulo);

        [OperationContract]
        [WebGet(UriTemplate = "SetInvNuevoTransito/?IDEdificio={IDEdificio}&IDUsuario={IDUsuario}&nombreEntrega={nombreEntrega}&nombreRecibe={nombreRecibe}&noInv={noInv}&noInvViejo={noInvViejo}&horaSalida={horaSalida}&Observaciones={Observaciones}&marca={marca}&modelo={modelo}&serie={serie}&caracteristicas={caracteristicas}&capitalizable={capitalizable}&TipoActivo={TipoActivo}", ResponseFormat = WebMessageFormat.Json)]
        Status SetEnviarInventarioNuevo(int IDEdificio, int IDUsuario, string nombreEntrega, string nombreRecibe, string noInv, string noInvViejo, string horaSalida, string Observaciones, string marca, string modelo, string serie, string caracteristicas, int capitalizable, int TipoActivo);

        [OperationContract]
        [WebGet(UriTemplate = "ObtenerTransitoEntregado/?IDEdificio={IDEdificio}", ResponseFormat = WebMessageFormat.Json)]
        ListaTransito GetTransitoEntregado(int IDEdificio);

        [OperationContract]
        [WebGet(UriTemplate = "ModificarTransitoEntregado/?IDEdificio={IDEdificio}&nombreRecibe={nombreRecibe}&noInv={noInv}", ResponseFormat = WebMessageFormat.Json)]
        Status SetRecibirInventario(int IDEdificio, string nombreRecibe, string noInv);

        [OperationContract]
        [WebGet(UriTemplate = "ObtenerEmpleadoDesc/?noempleado={noempleado}", ResponseFormat = WebMessageFormat.Json)]
        empleado getEmpleadoDesc(int noempleado);

        [OperationContract]
        [WebGet(UriTemplate = "GuardarResguardo/?noempleado={noempleado}&nombre={nombre}&area={area}&departamento={departamento}&usuarioid={usuarioid}&puesto={puesto}", ResponseFormat = WebMessageFormat.Json)]
        Status NuevoResguardo(int noempleado, string nombre, string area, string departamento, int usuarioid, string puesto);

        [OperationContract]
        [WebGet(UriTemplate = "GuardarArticuloResguardo/?resguardoID={resguardoID}&numinv={numinv}&usuarioid={usuarioid}", ResponseFormat = WebMessageFormat.Json)]
        Status NuevoResguardoDetalle(int resguardoID, string numinv, int usuarioid);

        [OperationContract]
        [WebGet(UriTemplate = "ObtenerResguardos/?noemp={noemp}", ResponseFormat = WebMessageFormat.Json)]
        ListaResguardoMovil GetListaResguardoMovil(int noemp);

        [OperationContract]
        [WebGet(UriTemplate = "ObtenerResguardoMovilDetalle/?ResguardoID={ResguardoID}", ResponseFormat = WebMessageFormat.Json)]
        ListaResguardoMovilDetalle GetListaResguardoMovilDetalle(int ResguardoID);

        [OperationContract]
        [WebGet(UriTemplate = "EliminarResguardoDetalle/?ResguardoDetalleID={ResguardoDetalleID}", ResponseFormat = WebMessageFormat.Json)]
        Status BorrarResguardoDetalle(int ResguardoDetalleID);

        [OperationContract]
        [WebGet(UriTemplate = "TerminarResguardo/?resguardoID={resguardoID}&usuarioid={usuarioid}", ResponseFormat = WebMessageFormat.Json)]
        Status TerminarResguardos(int resguardoID, int usuarioid);*/

        //CONSEJEROS
        [OperationContract]
        [WebGet(UriTemplate = "/geloginConsejero/?username={username}&password={password}", ResponseFormat = WebMessageFormat.Json)]
        ConsejeroID LoginConsejeros(string username, string password);

        [OperationContract]
        [WebGet(UriTemplate = "/getNombreConsejero/?clave={clave}&rfc={rfc}", ResponseFormat = WebMessageFormat.Json)]
        NombreConsejero GetNombreConsejero(string clave, string rfc);

        [OperationContract]
        [WebGet(UriTemplate = "setDocumentos/?_Pclaveelector={_Pclaveelector}&_Psrfc={_Psrfc}&actanacimiento={actanacimiento}&comprobante={comprobante}&credencialfrente={credencialfrente}&credencialatras={credencialatras}", ResponseFormat = WebMessageFormat.Json)]
        Status setDocumentos(string _Pclaveelector, string _Psrfc, string actanacimiento, string comprobante, string credencialfrente, string credencialatras);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/setImagen")]
        Status FileUploadDocumento(Stream stream);

        [OperationContract]
        [WebGet(UriTemplate = "setNombreDocumento/?_clave={clave}&tipo={tipo}", ResponseFormat = WebMessageFormat.Json)]
        Status renameFileDocumento(string clave, string tipo);

        [OperationContract]
        [WebGet(UriTemplate = "BuscarDocConsejero/clave={clave}", ResponseFormat = WebMessageFormat.Json)]
        ConsejeroID getConsejeroFromDocuments(string clave);

        //RULETA
        [OperationContract]
        [WebGet(UriTemplate = "SetUsuarioRuleta/?usuario={Usuario}&clave={clave}&correo={correo}&nivel={nivel}", ResponseFormat = WebMessageFormat.Json)]
        Status setUsuarioRuleta(string usuario, string clave, string correo, int nivel);

        [OperationContract]
        [WebGet(UriTemplate = "getloginRuleta/?usuario={usuario}&clave={clave}", ResponseFormat = WebMessageFormat.Json)]
        UsuariosRuletaID getloginRuleta(string usuario, string clave);

        [OperationContract]
        [WebGet(UriTemplate = "/getPreguntas", ResponseFormat = WebMessageFormat.Json)]
        preguntaarray GetPreguntas();

        [OperationContract]
        [WebGet(UriTemplate = "/getCategorias", ResponseFormat = WebMessageFormat.Json)]
        Categorias GetCategorias();

        //[OperationContract]
        //[WebGet(UriTemplate = "/setGanador/?usuario={usuario}&clave={clave}&validas={validas}", ResponseFormat = WebMessageFormat.Json)]
        //Status setUsuarioGanador(string usuario, string clave, int validas);

        [OperationContract]
        [WebGet(UriTemplate = "/setGanador/?usuario={usuario}&clave={clave}&validas={validas}", ResponseFormat = WebMessageFormat.Json)]
        Status setPuntosGanador(string usuario, string clave, int validas);

        [OperationContract]
        [WebGet(UriTemplate = "/getGanadores", ResponseFormat = WebMessageFormat.Json)]
        Ganadores GetGanadores();

        [OperationContract]
        [WebGet(UriTemplate = "/getGanadorID/?usuario={usuario}", ResponseFormat = WebMessageFormat.Json)]
        GanadorID GetGanadorID(String usuario);

        //[OperationContract]
        //[WebGet(UriTemplate = "UpdateGanador/?usuario={usuario}&clave={clave}&validas={validas}", ResponseFormat = WebMessageFormat.Json)]
        //Status UpdateGanador(string usuario, string clave, int validas);

        [OperationContract]
        [WebGet(UriTemplate = "ObtenerActivos", ResponseFormat = WebMessageFormat.Json)]
        ListaActivo GetActivos();

        [OperationContract]
        [WebGet(UriTemplate = "/RecuperarPassword/?correo={correo}", ResponseFormat = WebMessageFormat.Json)]
        Status EnviarCorreo(string correo);


    }

    public class Status
    {
        
        [DataMember]
        public string status { get; set; }

    }

    #region BienesM

    //                    ***Bienes Materiales***                //

    public class bienID
    {

        [DataMember]
        public int idbien;
        [DataMember]
        public int iduser;
        [DataMember]
        public string fechaalta;
        [DataMember]
        public string numinv;
        [DataMember]
        public string numinvNuevo;
        [DataMember]
        public int idactivo;
        [DataMember]
        public string marca;
        [DataMember]
        public string modelo;
        [DataMember]
        public string serie;
        [DataMember]
        public string nofactura;
        [DataMember]
        public string caracteristicas;
        [DataMember]
        public string observaciones;
        [DataMember]
        public int capitalizable;
        [DataMember]
        public int EdificioID;
        [DataMember]
        public int StatusID;
    }

    public class bienes
    {
        [DataMember]
        public int idbien;
        [DataMember]
        public int iduser;
        [DataMember]
        public string fechaalta;
        [DataMember]
        public string numinv;
        [DataMember]
        public string numinvNuevo;
        [DataMember]
        public int idactivo;
        [DataMember]
        public string marca;
        [DataMember]
        public string modelo;
        [DataMember]
        public string serie;
        [DataMember]
        public string caracteristicas;
        [DataMember]
        public int capitalizable;
    }

    public class bienresg
    {

        [DataMember]
        public int idbien;
        [DataMember]
        public int idresguardo;
        [DataMember]
        public int numemp;
        [DataMember]
        public string nombremp;
        [DataMember]
        public int iduser;
        [DataMember]
        public string fechaalta;
        [DataMember]
        public string numinv;
        [DataMember]
        public string numinvNuevo;
        [DataMember]
        public int idactivo;
        [DataMember]
        public string marca;
        [DataMember]
        public string modelo;
        [DataMember]
        public string serie;
        [DataMember]
        public string caracteristicas;
        [DataMember]
        public int is_new;
        [DataMember]
        public int is_cap;
        [DataMember]
        public int Estado;

    }

    public class bienResguardado
    {
        [DataMember]
        public int idbien;
        [DataMember]
        public int iduser;
        [DataMember]
        public string fechaalta;
        [DataMember]
        public string numinv;
        [DataMember]
        public string numinvNuevo;
        [DataMember]
        public int idactivo;
        [DataMember]
        public string marca;
        [DataMember]
        public string modelo;
        [DataMember]
        public string serie;
        [DataMember]
        public string caracteristicas;
        [DataMember]
        public int capitalizable;
        [DataMember]
        public int EdificioID;
    }

    public class BienTransito
    {
        [DataMember]
        public int TransitoID;
        [DataMember]
        public string NombreEntrega;
        [DataMember]
        public string NombreRecibe;
        [DataMember]
        public string numinv;
        [DataMember]
        public string numinvViejo;
        [DataMember]
        public string Observaciones;
        [DataMember]
        public int nuevo;
        [DataMember]
        public string marca;
        [DataMember]
        public string modelo;
        [DataMember]
        public string serie;
        [DataMember]
        public string caracteristicas;
    }

    public class BienNuevoEnTransito
    {
        [DataMember]
        public string numinv;
        [DataMember]
        public string numinvViejo;
        [DataMember]
        public string Observaciones;
        [DataMember]
        public int nuevo;
        [DataMember]
        public string marca;
        [DataMember]
        public string modelo;
        [DataMember]
        public string serie;
        [DataMember]
        public string caracteristicas;
        [DataMember]
        public int capitalizable;
        [DataMember]
        public int EdificioID;
        [DataMember]
        public int TipoActivo;
        [DataMember]
        public int EstatusTransito;
        [DataMember]
        public int UsuarioID;
    }

    public class empleado
    {
        [DataMember]
        public int EmpleadosID;
        [DataMember]
        public int noempleado;
        [DataMember]
        public int DepartamentoID;
        [DataMember]
        public int Idarea;
        [DataMember]
        public string nombre;
        [DataMember]
        public string areadesc;
        [DataMember]
        public string deptodesc;
        [DataMember]
        public string puesto;
    }

    public class Resguardo
    {
        [DataMember]
        public int ResguardoID;
        [DataMember]
        public int noempleado;
        [DataMember]
        public string nombreemp;
        [DataMember]
        public string areadesc;
        [DataMember]
        public string deptodesc;
        [DataMember]
        public string estatusresg;
        [DataMember]
        public string puestoresg;
    }

    public class ArticuloResguardo
    {
        [DataMember]
        public int ID;
        [DataMember]
        public int BienID;
        [DataMember]
        public string noinv;
        [DataMember]
        public string marca;
        [DataMember]
        public string modelo;
        [DataMember]
        public string serie;
        [DataMember]
        public string caracteristicas;
        [DataMember]
        public int CapitalizableID;
        [DataMember]
        public int usuarioID;
        [DataMember]
        public DateTime fecharegistro;
    }
    

    public class TipoActivo
    {
        [DataMember]
        public int TipoID;
        [DataMember]
        public string Descripcion;
    }

    [DataContract]
    public class ListaActivo
    {
        [DataMember]
        public List<TipoActivo> TipoActivos {get; set;}
    }

    [DataContract]
    public class bienresguardom
    {
        [DataMember]
        public List<bienresg> bienresgs {get; set;}
    }

    [DataContract]
    public class Bienesm
    {
        [DataMember]
        public List<bienes> bienesm { get; set; }
    }

    [DataContract]
    public class ListaResguardo
    {
        [DataMember]
        public List<bienResguardado> bienResguardados { get; set; }
    }

    [DataContract]
    public class ListaTransito
    {
        [DataMember]
        public List<BienTransito> bienesTransito { get; set; }
    }

    [DataContract]
    public class ListaResguardoMovil
    {
        [DataMember]
        public List<Resguardo> Resguardos {get; set;}
    }

    [DataContract]
    public class ListaResguardoMovilDetalle
    {
        
        [DataMember]
        public List<ArticuloResguardo> ArticuloResguardos { get; set; }

    }


    #endregion

    #region Consejeros
    //                     *** Login Consejeros ***                    //

    public class ConsejeroID
    {
        [DataMember]
        public string claveelector;
        [DataMember]
        public string rfc;
    }

    public class NombreConsejero
    {
        [DataMember]
        public string paterno;
        [DataMember]
        public string materno;
        [DataMember]
        public string nombre;
    }

    public class DocumentosC
    {
        [DataMember]
        public string actanacimiento;
        [DataMember]
        public string comprobanted;
        [DataMember]
        public string credencialfrente;
        [DataMember]
        public string credencialatras;
    }

    #endregion

    #region Usuarios
    //                     ***Servicios de Usuarios***                 //

    public class UsuarioID
    {

        [DataMember]
        public int iduser;
        [DataMember]
        public string username;
        [DataMember]
        public string password;
        [DataMember]
        public string nombre;
        [DataMember]
        public int idnivel;
        [DataMember]
        public string numemp;

    }

    public class Edificio
    {

        [DataMember]
        public int idedificio;
        [DataMember]
        public string nombre;

    }

    [DataContract]
    public class Edificiom
    {
        
        [DataMember]
        public List<Edificio> edificiom { set; get; }

    }




    #endregion

    #region Inventario

    //           ***Inventario Fisico*** 




    #endregion

    #region Ruleta

    public class preguntaarray
    {
        
        public List<Pregunta> preguntas { get; set; }

    }

    public class UsuariosRuletaID
    {
        [DataMember]
        public string usuario;
        [DataMember]
        public string clave;
        [DataMember]
        public string correo;
        [DataMember]
        public int nivel;
    }

    public class Categorias
    {
        
        public List<Categoria> categorias { get; set; }

    }

    public class Categoria
    { 
        [DataMember]
        public int ID;
        [DataMember]
        public string nombre;
    }

    public class Ganadores
    {
        
        public List<Ganador> ganadores { get; set; }

    }

    public class Ganador
    {

        [DataMember]
        public string usuario;
        [DataMember]
        public string clave;
        [DataMember]
        public int puntos;

    }

    public class GanadorID
    {
        [DataMember]
        public int rank;
        [DataMember]
        public string usuario;
        [DataMember]
        public string clave;
        [DataMember]
        public int puntos;
    }

    public class Pregunta
    {

        [DataMember]
        public int ID;
        [DataMember]
        public int categoriaID;
        [DataMember]
        public string descripcion;
        [DataMember]
        public List<Respuesta> respuestas;

    }

    public class Respuesta
    {
        [DataMember]
        public int ID;
        [DataMember]
        public int preguntaID;
        [DataMember]
        public string Descripcion;
        [DataMember]
        public int valida;
    }

    #endregion

}
