﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Globalization;

namespace CofuServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        //PRODUCCION
        public string StringConexion = "Data Source=192.168.254.2; Initial catalog=Sistema_Inventarios; User ID=chatpc; Password='pccha7';";
        //PRUEBAS
        //public string StringConexion = "Data Source=192.168.254.6; Initial catalog=Sistema_Inventarios; User ID=acceso; Password='dell2016';";

        public string StringConexionRH = "Data Source=192.168.254.2; Initial catalog=RecHumanosExpediente; User ID=rechumanos; Password='tres2013';";

        public string ConexionConsejeros = "Data Source=192.168.254.136; Initial catalog=Cons_2019; User ID=acceso; Password = 'dell2016';";

        public string ConexionRuleta = "Data Source=192.168.254.35; Initial catalog=Ruleta; User ID=acceso; Password = 'dell2016';";




        public static string GetSHA1(string str)
        {
            SHA1 sha1 = SHA1Managed.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = sha1.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++)
                sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

        #region Usuarios
        //                 ***Usuarios***                //

        public Status getLogin(string username, string password)
        {

            string resultado = "Error";
            int activo = 0;
            string query = "SELECT * FROM usuarios WHERE username='" + username + "' AND password='" + password + "' ";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    resultado = ds.Tables[0].Rows[0]["usuarioid"].ToString();
                    activo = Convert.ToInt16(ds.Tables[0].Rows[0]["estatususuario"]);
                }
            }
            catch
            { }
            conn.Close();
            if (activo == 0) {
                resultado = "Error";
            }
            return new Status { status = resultado };

        }

        //          ***Consejeros***            //


        public ConsejeroID LoginConsejeros(string username, string password)
        {

            ConsejeroID consid = new ConsejeroID();
            string query = "SELECT * FROM Cons_login WHERE _PClaveelector='" + username + "' AND _Psrfc='" + password + "' ";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionConsejeros;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    consid.claveelector = ds.Tables[0].Rows[0]["_PClaveelector"].ToString();
                    consid.rfc = ds.Tables[0].Rows[0]["_Psrfc"].ToString();
                }
            }
            catch
            { }

            conn.Close();

            return consid;

        }

        public NombreConsejero GetNombreConsejero(string clave, string rfc)
        {

            NombreConsejero nombreC = new NombreConsejero();
            string query = "SELECT * FROM Cons_DatosGenerales WHERE _PClaveelector='" + clave + "' AND _Psrfc='" + rfc + "' ";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionConsejeros;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    nombreC.paterno = ds.Tables[0].Rows[0]["spaterno"].ToString();
                    nombreC.materno = ds.Tables[0].Rows[0]["smaterno"].ToString();
                    nombreC.nombre = ds.Tables[0].Rows[0]["_snombre"].ToString();
                }
            }
            catch
            { }

            conn.Close();

            return nombreC;

        }

        public empleado getEmpleado(int noempleado)
        {

            empleado emp = new empleado();
            string query = "SELECT * FROM Empleados where noempleado ='" + noempleado + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexionRH;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    emp.EmpleadosID = Convert.ToInt16(ds.Tables[0].Rows[0]["EmpleadosID"]);
                    emp.noempleado = Convert.ToInt16(ds.Tables[0].Rows[0]["noempleado"]);
                    emp.DepartamentoID = Convert.ToInt16(ds.Tables[0].Rows[0]["DepartamentoID"]);
                    emp.Idarea = Convert.ToInt16(ds.Tables[0].Rows[0]["Idarea"]);
                    emp.nombre = ds.Tables[0].Rows[0]["nombre"].ToString();
                }
            }
            catch
            { }
            conn.Close();

            return emp;

        }

        public empleado getEmpleadoDesc(int noempleado)
        {

            empleado emp = new empleado();
            string query = "SELECT Empleados.EmpleadosID,Empleados.nombre,Empleados.puesto,Empleados.noempleado,Areas.area,Departamento.departamento from Empleados,Areas,Departamento where Empleados.Idarea = Areas.idarea and Empleados.DepartamentoID = Departamento.DepartamentoID and noempleado = " + noempleado;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    emp.EmpleadosID = Convert.ToInt16(ds.Tables[0].Rows[0]["EmpleadosID"]);
                    emp.noempleado = Convert.ToInt16(ds.Tables[0].Rows[0]["noempleado"]);
                    emp.nombre = ds.Tables[0].Rows[0]["nombre"].ToString();
                    emp.areadesc = ds.Tables[0].Rows[0]["area"].ToString();
                    emp.deptodesc = ds.Tables[0].Rows[0]["departamento"].ToString();
                    emp.puesto = ds.Tables[0].Rows[0]["puesto"].ToString();
                }
            }
            catch
            { }
            conn.Close();

            return emp;

        }


        public Status setDocumentos(string _Pclaveelector, string _Psrfc, string actanacimiento, string comprobante, string credencialfrente, string credencialatras)
        {

            string query = "INSERT INTO Cons_Documentos([_Pclaveelector],[_Psrfc],[Actanacimiento],[ComprobanteDomicilio],[Credencialvotacion_frente],[Credencialvotacion_atras]) VALUES (@_Pclaveelector,@_Psrfc,@actanacimiento,@comprobante,@credencialfrente,@credencialatras)";

            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionConsejeros;

            try
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddRange(new SqlParameter[]
                            {

                                new SqlParameter("@_Pclaveelector",_Pclaveelector),
                                new SqlParameter("@_Psrfc",_Psrfc),
                                new SqlParameter("@actanacimiento",actanacimiento),
                                new SqlParameter("@comprobante",comprobante),
                                new SqlParameter("@credencialfrente",credencialfrente),
                                new SqlParameter("@credencialatras",credencialatras)

                            });
                    cmd.ExecuteNonQuery();
                    resultado = "ok";
                }

            }
            catch { }
            conn.Close();
            return new Status { status = resultado };

        }

        public Status FileUploadDocumento(Stream stream)
        {
            byte[] buffer = new byte[1000000];
            stream.Read(buffer, 0, 1000000);

            FileStream f = new FileStream("C:\\inetpub\\wwwroot\\invapi\\documentosCon\\" + "Archivo" + ".jpg", FileMode.OpenOrCreate);

            f.Write(buffer, 0, buffer.Length);
            f.Close();
            stream.Close();
            return new Status { status = "http://201.159.19.252:8080/invapi/documentosCon/" + "Archivo" + ".jpg" };
        }

        public Status renameFileDocumento(string clave, string tipo)
        {
            string path = "C:\\inetpub\\wwwroot\\invapi\\documentosCon\\";
            string Fromfile = path + "Archivo.jpg";
            string Tofile = path + clave + "_" + tipo + ".jpg";
            File.Exists(Tofile);
            File.Move(Fromfile, Tofile);

            return new Status { status = "completado" };
        }

        public ConsejeroID getConsejeroFromDocuments(string clave)
        {

            ConsejeroID consid = new ConsejeroID();
            string query = "SELECT * FROM Cons_Documentos WHERE _PClaveelector='" + clave + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionConsejeros;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    consid.claveelector = ds.Tables[0].Rows[0]["_PClaveelector"].ToString();
                    consid.rfc = ds.Tables[0].Rows[0]["_Psrfc"].ToString();
                }
            }
            catch
            { }

            conn.Close();

            return consid;

        }


        #endregion


        #region BienesM
        //               ***Servicios Bienes Materiales***           //

        public bienID GetBienID(string numinv)
        {

            bienID bid = new bienID();
            string query = "SELECT * FROM Alta_Bien WHERE noinv_viejo=" + "'" + numinv + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                bid.idbien = Convert.ToInt16(ds.Tables[0].Rows[0]["Alta_BienID"]);
                bid.iduser = Convert.ToInt16(ds.Tables[0].Rows[0]["Usuarioid"]);
                bid.fechaalta = ds.Tables[0].Rows[0]["fechaaltabien"].ToString();
                bid.numinv = ds.Tables[0].Rows[0]["noinv_viejo"].ToString();
                bid.numinvNuevo = ds.Tables[0].Rows[0]["noinv_nuevo"].ToString();
                bid.marca = ds.Tables[0].Rows[0]["marca"].ToString();
                bid.modelo = ds.Tables[0].Rows[0]["modelo"].ToString();
                bid.serie = ds.Tables[0].Rows[0]["serie"].ToString();
                bid.nofactura = ds.Tables[0].Rows[0]["nofactura"].ToString();
                bid.idactivo = Convert.ToInt16(ds.Tables[0].Rows[0]["ACTIVO_ID"]);
                bid.caracteristicas = ds.Tables[0].Rows[0]["caracteristicas"].ToString();
                bid.observaciones = ds.Tables[0].Rows[0]["observaciones"].ToString();
                bid.capitalizable = Convert.ToInt16(ds.Tables[0].Rows[0]["CapitalizableID"]);
                bid.EdificioID = ObtenerIDeDIFICIO(bid.idbien);
            }
            catch
            {

            }



            return bid;
        }

        public int ObtenerIDeDIFICIO(int IDbien)
        {

            int EdificioID = 0;
            string query = "SELECT EdificioID FROM Alta_Bien_Detalle WHERE Alta_BienID= " + IDbien; 
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                EdificioID = Convert.ToInt16(ds.Tables[0].Rows[0]["EdificioID"]);
            }
            catch
            {
            }
            return EdificioID;

        }

        public bienID GetBienIDCAP(string numinv)
        {

            bienID bid = new bienID();
            string query = "SELECT * FROM Alta_Bien WHERE noinv_viejo=" + "'" + numinv + "' AND CapitalizableID = 2";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                bid.idbien = Convert.ToInt16(ds.Tables[0].Rows[0]["Alta_BienID"]);
                bid.iduser = Convert.ToInt16(ds.Tables[0].Rows[0]["Usuarioid"]);
                bid.fechaalta = ds.Tables[0].Rows[0]["fechaaltabien"].ToString();
                bid.numinv = ds.Tables[0].Rows[0]["noinv_viejo"].ToString();
                bid.numinvNuevo = ds.Tables[0].Rows[0]["noinv_nuevo"].ToString();
                bid.marca = ds.Tables[0].Rows[0]["marca"].ToString();
                bid.modelo = ds.Tables[0].Rows[0]["modelo"].ToString();
                bid.serie = ds.Tables[0].Rows[0]["serie"].ToString();
                bid.nofactura = ds.Tables[0].Rows[0]["nofactura"].ToString();
                bid.idactivo = Convert.ToInt16(ds.Tables[0].Rows[0]["ACTIVO_ID"]);
                bid.caracteristicas = ds.Tables[0].Rows[0]["caracteristicas"].ToString();
                bid.observaciones = ds.Tables[0].Rows[0]["observaciones"].ToString();
                bid.capitalizable = Convert.ToInt16(ds.Tables[0].Rows[0]["CapitalizableID"]);
            }
            catch
            {



            }
            return bid;
        }

        public bienID GetBienIDNUEVO(string numinv)
        {

            bienID bid = new bienID();
            string query = "SELECT * FROM Alta_Bien WHERE noinv_nuevo=" + "'" + numinv + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                bid.idbien = Convert.ToInt16(ds.Tables[0].Rows[0]["Alta_BienID"]);
                bid.iduser = Convert.ToInt16(ds.Tables[0].Rows[0]["Usuarioid"]);
                bid.fechaalta = ds.Tables[0].Rows[0]["fechaaltabien"].ToString();
                bid.numinv = ds.Tables[0].Rows[0]["noinv_viejo"].ToString();
                bid.numinvNuevo = ds.Tables[0].Rows[0]["noinv_nuevo"].ToString();
                bid.marca = ds.Tables[0].Rows[0]["marca"].ToString();
                bid.modelo = ds.Tables[0].Rows[0]["modelo"].ToString();
                bid.serie = ds.Tables[0].Rows[0]["serie"].ToString();
                bid.nofactura = ds.Tables[0].Rows[0]["nofactura"].ToString();
                bid.idactivo = Convert.ToInt16(ds.Tables[0].Rows[0]["ACTIVO_ID"]);
                bid.caracteristicas = ds.Tables[0].Rows[0]["caracteristicas"].ToString();
                bid.observaciones = ds.Tables[0].Rows[0]["observaciones"].ToString();
                bid.capitalizable = Convert.ToInt16(ds.Tables[0].Rows[0]["CapitalizableID"]);
                bid.EdificioID = ObtenerIDeDIFICIO(bid.idbien);
            }
            catch
            {



            }
            return bid;
        }

        public int GetBienFisico(string numinv , int noemFp)
        {

            bienID bid = new bienID();
            string query = "SELECT * FROM Resguardo_fisico WHERE noinv_nuevo=" + "'" + numinv + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                bid.idbien = Convert.ToInt16(ds.Tables[0].Rows[0]["IdResgFisico"]);
            }
            catch
            {



            }
            return bid.idbien;
        }

        public int GetResFisicoID(int Idbien)
        {

            bienID bid = new bienID();
            string query = "SELECT * FROM ResguardoDetalle WHERE Alta_BienID=" + "'" + Idbien + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                bid.idbien = Convert.ToInt16(ds.Tables[0].Rows[0]["Alta_BienID"]);
            }
            catch
            {



            }
            return bid.idbien;

        }


        public bienresg GetBienresg(int numinv, int noemp)
        {

            bienresg bin = new bienresg();
            string query = "Select * FROM Vresguardo where noempleadoresg = " + noemp + " AND Alta_BienID=" + numinv;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {

                bin.idbien = Convert.ToInt16(ds.Tables[0].Rows[0]["Alta_BienID"]);
                bin.idresguardo = Convert.ToInt16(ds.Tables[0].Rows[0]["ResguardoID"]);
                bin.nombremp = ds.Tables[0].Rows[0]["nombreemp"].ToString();
                bin.numemp = Convert.ToInt16(ds.Tables[0].Rows[0]["noempleadoresg"]);
                bin.iduser = Convert.ToInt16(ds.Tables[0].Rows[0]["Usuarioid"]);
                bin.fechaalta = ds.Tables[0].Rows[0]["fechaaltabien"].ToString();
                bin.numinv = ds.Tables[0].Rows[0]["noinv_viejo"].ToString();
                bin.marca = ds.Tables[0].Rows[0]["marca"].ToString();
                bin.modelo = ds.Tables[0].Rows[0]["modelo"].ToString();
                bin.serie = ds.Tables[0].Rows[0]["serie"].ToString();
                bin.idactivo = Convert.ToInt16(ds.Tables[0].Rows[0]["ACTIVO_ID"]);
                bin.caracteristicas = ds.Tables[0].Rows[0]["caracteristicas"].ToString();

            }
            catch { }

            return bin;



        }


        public Bienesm GetBienesm()
        {

            Bienesm bin = new Bienesm();
            bin.bienesm = new List<bienes>();

            String query = "SELECT * FROM Alta_Bien where CapitalizableID = 1";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    bienes b = new bienes();
                    b.idbien = Convert.ToInt16(ds.Tables[0].Rows[i]["Alta_BienID"]);
                    b.iduser = Convert.ToInt16(ds.Tables[0].Rows[i]["Usuarioid"]);
                    b.fechaalta = ds.Tables[0].Rows[i]["fechaaltabien"].ToString();
                    b.numinv = ds.Tables[0].Rows[i]["noinv_viejo"].ToString();
                    b.numinvNuevo = ds.Tables[0].Rows[i]["noinv_nuevo"].ToString();
                    b.marca = ds.Tables[0].Rows[i]["marca"].ToString();
                    b.modelo = ds.Tables[0].Rows[i]["modelo"].ToString();
                    b.serie = ds.Tables[0].Rows[i]["serie"].ToString();
                    b.idactivo = Convert.ToInt16(ds.Tables[0].Rows[i]["ACTIVO_ID"]);
                    b.caracteristicas = ds.Tables[0].Rows[i]["caracteristicas"].ToString();
                    b.capitalizable = Convert.ToInt16(ds.Tables[0].Rows[i]["CapitalizableID"]);
                    bin.bienesm.Add(b);

                }
            }
            catch { }

            return bin;
        }

        public ListaResguardo GetBienesEdificio(string edificioID)
        {

            ListaResguardo bin = new ListaResguardo();
            bin.bienResguardados = new List<bienResguardado>();

            String query = "SELECT * FROM Alta_Bien, ResguardoDetalle WHERE Alta_Bien.Alta_BienID = ResguardoDetalle.Alta_BienID AND EdificioID = "+ edificioID;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    bienResguardado b = new bienResguardado();
                    b.idbien = Convert.ToInt16(ds.Tables[0].Rows[i]["Alta_BienID"]);
                    b.iduser = Convert.ToInt16(ds.Tables[0].Rows[i]["Usuarioid"]);
                    b.fechaalta = ds.Tables[0].Rows[i]["fechaaltabien"].ToString();
                    b.numinv = ds.Tables[0].Rows[i]["noinv_viejo"].ToString();
                    b.numinvNuevo = ds.Tables[0].Rows[i]["noinv_nuevo"].ToString();
                    b.marca = ds.Tables[0].Rows[i]["marca"].ToString();
                    b.modelo = ds.Tables[0].Rows[i]["modelo"].ToString();
                    b.serie = ds.Tables[0].Rows[i]["serie"].ToString();
                    b.idactivo = Convert.ToInt16(ds.Tables[0].Rows[i]["ACTIVO_ID"]);
                    b.caracteristicas = ds.Tables[0].Rows[i]["caracteristicas"].ToString();
                    b.capitalizable = Convert.ToInt16(ds.Tables[0].Rows[i]["CapitalizableID"]);

                    bin.bienResguardados.Add(b);
                }
            }
            catch { }

            return bin;

        }

        public Bienesm GetBienesmCAP()
        {

            Bienesm bin = new Bienesm();
            bin.bienesm = new List<bienes>();

            String query = "SELECT * FROM Alta_Bien where CapitalizableID = 2";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    bienes b = new bienes();
                    b.idbien = Convert.ToInt16(ds.Tables[0].Rows[i]["Alta_BienID"]);
                    b.iduser = Convert.ToInt16(ds.Tables[0].Rows[i]["Usuarioid"]);
                    b.fechaalta = ds.Tables[0].Rows[i]["fechaaltabien"].ToString();
                    b.numinv = ds.Tables[0].Rows[i]["noinv_viejo"].ToString();
                    b.numinvNuevo = ds.Tables[0].Rows[i]["noinv_nuevo"].ToString();
                    b.marca = ds.Tables[0].Rows[i]["marca"].ToString();
                    b.modelo = ds.Tables[0].Rows[i]["modelo"].ToString();
                    b.serie = ds.Tables[0].Rows[i]["serie"].ToString();
                    b.idactivo = Convert.ToInt16(ds.Tables[0].Rows[i]["ACTIVO_ID"]);
                    b.caracteristicas = ds.Tables[0].Rows[i]["caracteristicas"].ToString();
                    b.capitalizable = Convert.ToInt16(ds.Tables[0].Rows[i]["CapitalizableID"]);
                    bin.bienesm.Add(b);

                }
            }
            catch { }

            return bin;
        }

        public bienresguardom GetBienresguardom(string noemp)
        {
            bienresguardom bin = new bienresguardom();
            bin.bienresgs = new List<bienresg>();

            String query = "Select * FROM Vresguardo where noempleadoresg = " + noemp;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    bienresg b = new bienresg();
                    b.idbien = Convert.ToInt16(ds.Tables[0].Rows[i]["Alta_BienID"]);
                    b.idresguardo = Convert.ToInt16(ds.Tables[0].Rows[i]["ResguardoID"]);
                    b.nombremp = ds.Tables[0].Rows[i]["nombreemp"].ToString();
                    b.numemp = Convert.ToInt16(ds.Tables[0].Rows[i]["noempleadoresg"]);
                    b.iduser = Convert.ToInt16(ds.Tables[0].Rows[i]["Usuarioid"]);
                    b.fechaalta = ds.Tables[0].Rows[i]["fechaaltabien"].ToString();
                    b.numinv = ds.Tables[0].Rows[i]["noinv_viejo"].ToString();
                    b.marca = ds.Tables[0].Rows[i]["marca"].ToString();
                    b.modelo = ds.Tables[0].Rows[i]["modelo"].ToString();
                    b.serie = ds.Tables[0].Rows[i]["serie"].ToString();
                    b.idactivo = Convert.ToInt16(ds.Tables[0].Rows[i]["ACTIVO_ID"]);
                    b.caracteristicas = ds.Tables[0].Rows[i]["caracteristicas"].ToString();
                    bin.bienresgs.Add(b);

                }
            }
            catch { }

            return bin;
        }


        public bienresguardom GetInventarioFisico(string noemp)
        {

            bienresguardom bin = new bienresguardom();
            bin.bienresgs = new List<bienresg>();

            String query = "Select * FROM View_inv_fisico where noempleado =" + noemp + "ORDER BY IdResgFisico DESC";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    bienresg b = new bienresg();
                    b.idbien = Convert.ToInt16(ds.Tables[0].Rows[i]["Alta_BienID"]);
                    b.idresguardo = Convert.ToInt16(ds.Tables[0].Rows[i]["IdResgFisico"]);
                    b.numemp = Convert.ToInt16(ds.Tables[0].Rows[i]["noempleado"]);
                    b.numinv = ds.Tables[0].Rows[i]["noinv_viejo"].ToString();
                    b.numinvNuevo = ds.Tables[0].Rows[i]["noinv_nuevo"].ToString();
                    b.marca = ds.Tables[0].Rows[i]["marca"].ToString();
                    b.modelo = ds.Tables[0].Rows[i]["modelo"].ToString();
                    b.serie = ds.Tables[0].Rows[i]["serie"].ToString();
                    b.idactivo = Convert.ToInt16(ds.Tables[0].Rows[i]["ACTIVO_ID"]);
                    b.caracteristicas = ds.Tables[0].Rows[i]["caracteristicas"].ToString();
                    b.is_new = Convert.ToInt16(ds.Tables[0].Rows[i]["Is_new"]);
                    b.is_cap = Convert.ToInt16(ds.Tables[0].Rows[i]["is_cap"]);
                    b.Estado = Convert.ToInt16(ds.Tables[0].Rows[i]["Estado"]);
                    bin.bienresgs.Add(b);
                }
            }
            catch { }

            return bin;

        }



        public Status setBien(int IDuser, string numinv,string numinvnuevo, string marca, string modelo, string serie, int nofactura, string caracteristicas, string observaciones,int Capitalizable,int EdificioID,int TipoActivo)
        {

            caracteristicas = caracteristicas.Replace("_", " ");
            observaciones = observaciones.Replace("_", " ");

            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;

            //AGREGAR A ALTA BIEN
            try
            {
                DateTime fecharegistro = DateTime.Now;
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO Alta_Bien([Usuarioid],[fechaaltabien],[noinv_nuevo],[noinv_viejo],[ACTIVO_ID],[marca],[modelo],[serie],[nofactura],[caracteristicas],[observaciones],[CapitalizableID],[EstatusBienID],[EdificioID]) VALUES (@Usuarioid,@fechaaltabien,@noinv_nuevo, @noinv_viejo, @ACTIVO_ID, @marca, @modelo, @serie, @nofactura, @caracteristicas, @observaciones,@CapitalizableID,1,@EdificioID)";
                    cmd.Parameters.AddRange(new SqlParameter[]
                            {

                                new SqlParameter("@Usuarioid", IDuser),
                                new SqlParameter("@fechaaltabien", fecharegistro),
                                new SqlParameter("@noinv_nuevo", numinvnuevo),
                                new SqlParameter("@noinv_viejo", numinv),
                                new SqlParameter("@ACTIVO_ID", TipoActivo),
                                new SqlParameter("@marca", marca),
                                new SqlParameter("@modelo", modelo),
                                new SqlParameter("@serie", serie),
                                new SqlParameter("@nofactura", nofactura),
                                new SqlParameter("@caracteristicas", caracteristicas),
                                new SqlParameter("@observaciones", observaciones),
                                new SqlParameter("@CapitalizableID",Capitalizable),
                                new SqlParameter("@EdificioID",EdificioID)
                            });
                    cmd.ExecuteNonQuery();
                    //resultado = "ok";
                }

            }
            catch { }
            conn.Close();

            return new Status { status = resultado };
        }

        public int getStatusBien(int Idbien)
        {
            bienID bid = new bienID();
            string query = "SELECT * FROM Alta_Bien WHERE Alta_BienID=" + "'" + Idbien + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                bid.idbien = Convert.ToInt16(ds.Tables[0].Rows[0]["EstatusBienID"]);
            }
            catch
            {



            }
            return bid.idbien;
        }

        public Status setinventario(int noempleado, int idbien, int idresguardo, int isnew, string noinv, int iscap,string noinvnuevo,int EdificioID,int idusuario)
        {

            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;

            int valor = GetBienFisico(noinvnuevo, noempleado);

            if (valor == 0)
            {

                int isresguardo = GetResFisicoID(idbien);

                if (isresguardo != 0)
                {
                    resultado = "Resguardado";
                }
                else
                {

                    int isvigente = getStatusBien(idbien);
                    if (isvigente == 1 || isvigente == 0)
                    {
                        try
                        {
                            DateTime fecharegistro = DateTime.Now;

                            conn.Open();
                            using (SqlCommand cmd = conn.CreateCommand())
                            {
                                cmd.CommandText = "INSERT INTO Resguardo_fisico([noempleado],[Alta_BienID],[ResguardoID],[Is_new],[noinv_viejo],[is_cap],[noinv_nuevo],EdificioID,[usuarioid],[Fecha_Registro],[Estado]) VALUES (@noempleado,@Alta_BienID,@ResguardoID,@Is_new,@noinv_viejo,@is_cap,@noinv_nuevo,@EdificioID,@usuarioid,@fecha_registro,1)";
                                cmd.Parameters.AddRange(new SqlParameter[]
                                        {

                                             new SqlParameter("@noempleado",noempleado),
                                             new SqlParameter("@Alta_BienID",idbien),
                                             new SqlParameter("@ResguardoID",idresguardo),
                                             new SqlParameter("@Is_new",isnew),
                                             new SqlParameter("@noinv_viejo",noinv),
                                             new SqlParameter("@is_cap",iscap),
                                             new SqlParameter("@noinv_nuevo",noinvnuevo),
                                             new SqlParameter("@EdificioID",EdificioID),
                                             new SqlParameter("@usuarioid",idusuario),
                                             new SqlParameter("@fecha_registro",fecharegistro)

                                        });
                                cmd.ExecuteNonQuery();
                                resultado = "ok";
                            }

                        }
                        catch { }
                        conn.Close();
                    }
                    else
                    {
                        resultado = "Novigente";
                    }
                    
                }
                
            }
            else
            {
                resultado = "existe";
            }
            
            return new Status { status = resultado };

        }

        // *************    Enviar/Recibir Inventarios  *************

        public Status setEnviarInventario(int IDEdificio, int IDUsuario, string nombreEntrega, string nombreRecibe, string noInv, string horaSalida, string Observaciones)
        {
            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
           bienID bien = new bienID();

            bien = BuscarBien(noInv);
            if (bien.idbien > 0)
            {

                int banderaTrans = BuscarInventarioTransito(IDEdificio, noInv);
                if (banderaTrans == 0)
                {
                    try
                    {
                        DateTime fecharegistro = DateTime.Now;
                        conn.Open();
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = "INSERT INTO TrasladodeBienes([Usuarioid],[EdificioID],[NomEntrega],[NomRecibe],[inventarionuevotraslado],[inventarioviejotraslado],[fechatraslado],[horatraslado],[EstatusTrasladoID],[observacionestraslado],[nuevo],[marcatraslado],[modelotraslado],[serietraslado],[Caracteristicastraslado],[CapitalizableID],[ACTIVO_ID],[EstatusBienID],[observacionesbines]) VALUES (@Usuarioid,@EdificioID,@NomEntrega,@NomRecibe,@inventarionuevotraslado,@inventarioviejotraslado,@fechatraslado,@horatraslado,@EstatusTrasladoID,@observacionestraslado,0,@marcatraslado,@modelotraslado,@serietraslado,@Caracteristicastraslado,@CapitalizableID,@ACTIVO_ID,@EstatusBienID,@observacionesbines)";
                            cmd.Parameters.AddRange(new SqlParameter[]
                                    {
                                             new SqlParameter("@Usuarioid",IDUsuario),
                                             new SqlParameter("@EdificioID",IDEdificio),
                                             new SqlParameter("@NomEntrega",nombreEntrega),
                                             new SqlParameter("@NomRecibe",nombreRecibe),
                                             new SqlParameter("@inventarionuevotraslado",noInv),
                                             new SqlParameter("@inventarioviejotraslado",bien.numinv),
                                             new SqlParameter("@fechatraslado",fecharegistro),
                                             new SqlParameter("@horatraslado",horaSalida),
                                             new SqlParameter("@EstatusTrasladoID",1),
                                             new SqlParameter("@observacionestraslado",Observaciones),
                                             new SqlParameter("@marcatraslado",bien.marca),
                                             new SqlParameter("@modelotraslado",bien.modelo),
                                             new SqlParameter("@serietraslado",bien.serie),
                                             new SqlParameter("@Caracteristicastraslado",bien.caracteristicas),
                                             new SqlParameter("@CapitalizableID",bien.capitalizable),
                                             new SqlParameter("@ACTIVO_ID",bien.idactivo),
                                             new SqlParameter("@EstatusBienID",bien.StatusID),
                                             new SqlParameter("@observacionesbines",bien.observaciones)


                                    });
                            cmd.ExecuteNonQuery();
                            resultado = "ok";
                        }
                    }
                    catch
                    { 
                    }
                    conn.Close();
                }
                else
                {
                    resultado = "EnTransito";
                }

            }
            else
            {

                resultado = "NoExiste";

            }

            return new Status { status = resultado };

        }

        public Status SetRecibirInventario(int IDEdificio,string nombreRecibe, string noInv)
        {
            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            int banderaTrans = BuscarInventarioTransitoRecibido(noInv);

            if (banderaTrans == 1)
            {
                BienNuevoEnTransito bien = new BienNuevoEnTransito();
                bien = ObtenerBienEntransito(noInv);
                //int banderaEstatus = RevisarEstatusTransito(noInv);
                if (bien.EstatusTransito == 1)
                {
                    //int esnuevo = ObtenerNuevo(noInv);
                    if (bien.nuevo == 1)
                    {
                        //MODIFICAR TABLA TRANSITO
                        try
                        {
                            //MODIFICAR TABLA TRANSITO
                            conn.Open();
                            using (SqlCommand cmd = conn.CreateCommand())
                            {
                                cmd.CommandText = "UPDATE TrasladodeBienes SET EstatusTrasladoID = 2,EdificioID = @EdificioID,NomRecibe = @NomRecibe WHERE inventarionuevotraslado = @inventarionuevotraslado";
                                cmd.Parameters.AddRange(new SqlParameter[]
                                        {
                                             new SqlParameter("@EdificioID",IDEdificio),
                                             new SqlParameter("@NomRecibe",nombreRecibe),
                                             new SqlParameter("@inventarionuevotraslado",noInv)
                                        });
                                cmd.ExecuteNonQuery();
                            }
                        }
                        catch
                        { }
                        conn.Close();

                        //AGREGAR A ALTA_BIEN
                        try
                        {
                            DateTime fecharegistro = DateTime.Now;
                            conn.Open();
                            using (SqlCommand cmd = conn.CreateCommand())
                            {
                                cmd.CommandText = "INSERT INTO Alta_Bien([Usuarioid],[noinv_nuevo],[noinv_viejo],[ACTIVO_ID],[marca],[modelo],[serie],[caracteristicas],[observaciones],[CapitalizableID],[EstatusBienID],[fechaaltabien],[EdificioID]) VALUES (@Usuarioid, @noinv_nuevo, @noinv_viejo, @ACTIVO_ID, @marca, @modelo, @serie,@caracteristicas, @observaciones,@CapitalizableID,@EstatusBienID,@fechaaltabien,@EdificioID)";
                                cmd.Parameters.AddRange(new SqlParameter[]
                                        {
                                             new SqlParameter("@Usuarioid",bien.UsuarioID),
                                             new SqlParameter("@noinv_nuevo",bien.numinv),
                                             new SqlParameter("@noinv_viejo",bien.numinvViejo),
                                             new SqlParameter("@ACTIVO_ID", bien.TipoActivo),
                                             new SqlParameter("@marca", bien.marca),
                                             new SqlParameter("@modelo", bien.modelo),
                                             new SqlParameter("@serie", bien.serie),
                                             new SqlParameter("@caracteristicas", bien.caracteristicas),
                                             new SqlParameter("@observaciones", bien.Observaciones),
                                             new SqlParameter("@CapitalizableID", bien.capitalizable),
                                             new SqlParameter("@EstatusBienID", 1),
                                             new SqlParameter("@fechaaltabien", fecharegistro),
                                             new SqlParameter("@EdificioID", IDEdificio)
                                        });
                                cmd.ExecuteNonQuery();
                            }
                        }
                        catch
                        { }
                        conn.Close();
                    }
                    else
                    {
                        int AltaBienID = ObtenerAltaBienID(noInv);
                        try
                        {
                            //MODIFICAR TABLA TRANSITO
                            conn.Open();
                            using (SqlCommand cmd = conn.CreateCommand())
                            {
                                cmd.CommandText = "UPDATE TrasladodeBienes SET EstatusTrasladoID = 2,EdificioID = @EdificioID,NomRecibe = @NomRecibe WHERE inventarionuevotraslado = @inventarionuevotraslado";
                                cmd.Parameters.AddRange(new SqlParameter[]
                                        {
                                             new SqlParameter("@EdificioID",IDEdificio),
                                             new SqlParameter("@NomRecibe",nombreRecibe),
                                             new SqlParameter("@inventarionuevotraslado",noInv)
                                        });
                                cmd.ExecuteNonQuery();
                            }

                        }
                        catch
                        { }
                        conn.Close();

                        try
                        {
                            //MODIFICAR TABLA DE ALTA_BIENES
                            conn.Open();
                            using (SqlCommand cmd = conn.CreateCommand())
                            {
                                cmd.CommandText = "UPDATE Alta_Bien SET EdificioID = @EdificioID WHERE Alta_BienID = @Alta_BienID";
                                cmd.Parameters.AddRange(new SqlParameter[]
                                        {
                                             new SqlParameter("@EdificioID",IDEdificio),
                                             new SqlParameter("@Alta_BienID",AltaBienID)
                                        });
                                cmd.ExecuteNonQuery();
                                resultado = "ok";
                            }

                        }
                        catch
                        { }
                        conn.Close();
                    }
                }
                else
                {
                    resultado = "Entregado";
                }

            }
            else
            {

                resultado = "NoExiste";

            }

            return new Status { status = resultado };

        }

        public BienNuevoEnTransito ObtenerBienEntransito(string noinv)
        {
            BienNuevoEnTransito bt = new BienNuevoEnTransito();
            string query = "SELECT * FROM TrasladodeBienes WHERE inventarionuevotraslado=" + "'" + noinv + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                bt.numinv = ds.Tables[0].Rows[0]["inventarionuevotraslado"].ToString();
                bt.numinvViejo = ds.Tables[0].Rows[0]["inventarioviejotraslado"].ToString();
                bt.Observaciones = ds.Tables[0].Rows[0]["observacionestraslado"].ToString();
                bt.nuevo = Convert.ToInt16(ds.Tables[0].Rows[0]["nuevo"]);
                bt.marca = ds.Tables[0].Rows[0]["marcatraslado"].ToString();
                bt.modelo = ds.Tables[0].Rows[0]["modelotraslado"].ToString();
                bt.serie = ds.Tables[0].Rows[0]["serietraslado"].ToString();
                bt.caracteristicas = ds.Tables[0].Rows[0]["Caracteristicastraslado"].ToString();
                bt.EstatusTransito = Convert.ToInt16(ds.Tables[0].Rows[0]["EstatusTrasladoID"]);
                bt.EdificioID = Convert.ToInt16(ds.Tables[0].Rows[0]["EdificioID"]);
                bt.UsuarioID = Convert.ToInt16(ds.Tables[0].Rows[0]["Usuarioid"]);
                bt.capitalizable = Convert.ToInt16(ds.Tables[0].Rows[0]["CapitalizableID"]);
                bt.TipoActivo = Convert.ToInt16(ds.Tables[0].Rows[0]["ACTIVO_ID"]);
            }
            catch
            { }

            return bt;
        }

        //**************** Resguardos*****************//

        public Status NuevoResguardo(int noempleado,string nombre,string area,string departamento,int usuarioid,string puesto)
        {
            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;

            //GUARDAR RESGUARDO
            try
            {

                DateTime fecharegistro = DateTime.Now;
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO ResguardoMovil ([fecharesg],[estastusresg],[noempleadoresg],[nombreemp],[areadesc],[deptoresg],[Usuarioid],[puesto]) output INSERTED.ResguardoID VALUES (@fecharesg,'A',@noempleadoresg,@nombreemp,@areadesc,@deptoresg,@Usuarioid,@puesto)";
                    cmd.Parameters.AddRange(new SqlParameter[]
                            {
                                             new SqlParameter("@fecharesg", fecharegistro),
                                             new SqlParameter("@noempleadoresg", noempleado),
                                             new SqlParameter("@nombreemp", nombre),
                                             new SqlParameter("@areadesc", area),
                                             new SqlParameter("@deptoresg", departamento),
                                             new SqlParameter("@Usuarioid", usuarioid),
                                             new SqlParameter("@puesto", puesto)
                            });
                    resultado = cmd.ExecuteScalar().ToString();
                }
            }
            catch
            { }
            conn.Close();

            return new Status { status = resultado };
        }

        public Status NuevoResguardoDetalle(int resguardoID,string numinv, int usuarioid)
        {
            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            bienID b = new bienID();
            b = BuscarBien(numinv);

            //GUARDAR RESGUARDO DETALLE
                       try
                       {

                            DateTime fecharegistro = DateTime.Now;
                            conn.Open();
                           using (SqlCommand cmd = conn.CreateCommand())
                            {
                                cmd.CommandText = "INSERT INTO ResguardoMovilDetalle ([ResguardoID],[Alta_BienID],[EdificioID],[fechaestatusbien],[Usuarioid],[CapitalizableID]) VALUES (@ResguardoID,@Alta_BienID,@EdificioID,@fechaestatusbien,@Usuarioid,@CapitalizableID)";
                                cmd.Parameters.AddRange(new SqlParameter[]
                                        {
                                                         new SqlParameter("@ResguardoID", resguardoID),
                                                         new SqlParameter("@Alta_BienID", b.idbien),
                                                         new SqlParameter("@EdificioID", b.EdificioID),
                                                         new SqlParameter("@fechaestatusbien", fecharegistro),
                                                         new SqlParameter("@Usuarioid", usuarioid),
                                                         new SqlParameter("@CapitalizableID", b.capitalizable)
                                        });
                                cmd.ExecuteNonQuery();
                                resultado = "ok";
                            }
                       }
                       catch
                       { }
                       conn.Close();
              

            return new Status { status = resultado };
        }

        public Status TerminarResguardos(int resguardoID, int usuarioid)
        {

            ListaResguardoMovilDetalle la = new ListaResguardoMovilDetalle();
            la.ArticuloResguardos = new List<ArticuloResguardo>();

            ListaResguardoMovilDetalle lb = new ListaResguardoMovilDetalle();
            lb.ArticuloResguardos = new List<ArticuloResguardo>();

            Int32 ResguardoA, ResguardoB = 0;
            int IdResINV, idResNC = 0;

            string resultado = "Error";
            String query = "Select ResguardoMovilDetalle.ResguardoDetalleID,ResguardoMovilDetalle.Alta_BienID,Alta_Bien.noinv_nuevo,Alta_Bien.marca,Alta_Bien.modelo,Alta_Bien.serie,Alta_Bien.caracteristicas,ResguardoMovilDetalle.CapitalizableID,ResguardoMovilDetalle.Usuarioid,ResguardoMovilDetalle.fechaestatusbien FROM ResguardoMovilDetalle,Alta_Bien WHERE ResguardoMovilDetalle.Alta_BienID = Alta_Bien.Alta_BienID AND ResguardoID = " + resguardoID + " ORDER BY fechaestatusbien DESC";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            conn.Close();

            string formato = "dd/MM/yyyy HH:mm:ss ";
            CultureInfo provider = CultureInfo.InvariantCulture;
            //CONTRUIR  LISTAS DE BIENES POR RESGUARDO

          //  try
           // {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ArticuloResguardo A = new ArticuloResguardo();
                    A.ID = Convert.ToInt16(ds.Tables[0].Rows[i]["ResguardoDetalleID"]);
                    A.BienID = Convert.ToInt16(ds.Tables[0].Rows[i]["Alta_BienID"]);
                    A.noinv = ds.Tables[0].Rows[i]["noinv_nuevo"].ToString();
                    A.marca = ds.Tables[0].Rows[i]["marca"].ToString();
                    A.modelo = ds.Tables[0].Rows[i]["modelo"].ToString();
                    A.serie = ds.Tables[0].Rows[i]["serie"].ToString();
                    A.caracteristicas = ds.Tables[0].Rows[i]["caracteristicas"].ToString();
                    A.usuarioID = Convert.ToInt16(ds.Tables[0].Rows[i]["Usuarioid"]);
                    A.fecharegistro = Convert.ToDateTime(ds.Tables[0].Rows[i]["fechaestatusbien"]);
                    A.CapitalizableID = Convert.ToInt16(ds.Tables[0].Rows[i]["CapitalizableID"]);
                    if (A.CapitalizableID == 1)
                    {
                        la.ArticuloResguardos.Add(A); 
                    }
                    else
                    {
                        lb.ArticuloResguardos.Add(A);
                    }
                }

                // ObtenerInformacion a Vaciar
                ResguardoA = la.ArticuloResguardos.Count;
                ResguardoB = lb.ArticuloResguardos.Count;
                Resguardo r = new Resguardo();
                r = ObtenerResguardoID(resguardoID);

                //Verificar tamaño del arreglo para guardar
                if (ResguardoA > 0)
                {
                    int noresginv = ObtenerNoResguardo(1);
                    DateTime fecharegistro = DateTime.Now;
                    conn.Open();
                    using (SqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "INSERT INTO Resguardo_Inventario ([noresg],[fecharesg],[noempleadoresg],[nombreemp],[Usuarioid],[puesto],[fechaaltaresinv],[EstatusRegistroResguardoID]) output INSERTED.ResguardoID VALUES (@noresg,@fecharesg,@noempleadoresg,@nombreemp,@Usuarioid,@puesto,@fechaaltaresinv,1)";
                        cmd.Parameters.AddRange(new SqlParameter[]
                                {
                                                         new SqlParameter("@noresg", noresginv),
                                                         new SqlParameter("@fecharesg", fecharegistro),
                                                         new SqlParameter("@noempleadoresg", r.noempleado),
                                                         new SqlParameter("@nombreemp", r.nombreemp),
                                                         new SqlParameter("@Usuarioid", usuarioid),
                                                         new SqlParameter("@puesto", r.puestoresg),
                                                         new SqlParameter("@fechaaltaresinv", fecharegistro)
                                });
                        IdResINV = Convert.ToInt32(cmd.ExecuteScalar());
                    }
                    conn.Close();

                    for (int i = 0; i < ResguardoA; i++)
                    {

                        conn.Open();
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = "INSERT INTO Resguardo_InventarioDetalle ([ResguardoID],[Alta_BienID],[Fecharegistro],[EstatusRegistroResguardoID],[Usuarioid]) VALUES (@ResguardoID,@Alta_BienID,@Fecharegistro,1,@Usuarioid)";
                            cmd.Parameters.AddRange(new SqlParameter[]
                                    {
                                                         new SqlParameter("@ResguardoID", IdResINV),
                                                         new SqlParameter("@Alta_BienID", la.ArticuloResguardos[i].BienID),
                                                         new SqlParameter("@Fecharegistro", la.ArticuloResguardos[i].fecharegistro),
                                                         new SqlParameter("@Usuarioid", la.ArticuloResguardos[i].usuarioID)
                                    });
                            cmd.ExecuteNonQuery();
                        }
                        conn.Close();

                    }
                }

                if (ResguardoB > 0)
                {
                    int noresnc = ObtenerNoResguardo(2);
                    DateTime fecharegistro = DateTime.Now;
                    conn.Open();
                    using (SqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "INSERT INTO Resguardo_NoControl ([noresgcontrol],[fechaaltaresgnoctrl],[noempleadorsgcontrol],[nombreeempleadoresgcontrol],[Usuarioid],[Puestoresgnocontrol],[EstatusRegistroResguardoID]) output INSERTED.Resguardo_NoControlID VALUES (@noresgcontrol,@fecharesg,@noempleadoresg,@nombreemp,@Usuarioid,@puesto,1)";
                        cmd.Parameters.AddRange(new SqlParameter[]
                                {
                                                         new SqlParameter("@noresgcontrol", noresnc),
                                                         new SqlParameter("@fecharesg", fecharegistro),
                                                         new SqlParameter("@noempleadoresg", r.noempleado),
                                                         new SqlParameter("@nombreemp", r.nombreemp),
                                                         new SqlParameter("@Usuarioid", usuarioid),
                                                         new SqlParameter("@puesto", r.puestoresg)
                                });
                        idResNC = Convert.ToInt32(cmd.ExecuteScalar());
                    }
                    conn.Close();

                    for (int i = 0; i < ResguardoB; i++)
                    {
                        conn.Open();
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = "INSERT INTO Resguardo_NoControlDetalla ([Resguardo_NoControlID],[Alta_BienID],[fechaaltaregnoctrl],[EstatusRegistroResguardoID],[Usuarioid]) VALUES (@ResguardoID,@Alta_BienID,@Fecharegistro,1,@Usuarioid)";
                            cmd.Parameters.AddRange(new SqlParameter[]
                                    {
                                                         new SqlParameter("@ResguardoID", idResNC),
                                                         new SqlParameter("@Alta_BienID", lb.ArticuloResguardos[i].BienID),
                                                         new SqlParameter("@Fecharegistro", lb.ArticuloResguardos[i].fecharegistro),
                                                         new SqlParameter("@Usuarioid", lb.ArticuloResguardos[i].usuarioID)
                                    });
                            cmd.ExecuteNonQuery();
                        }
                        conn.Close();
                    }
                }
                
                //UPDATE Estado del Resguardo Movil
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "UPDATE ResguardoMovil SET estastusresg = 'C' WHERE ResguardoID = @ResguardoID";
                    cmd.Parameters.AddRange(new SqlParameter[]
                            {
                                new SqlParameter("@ResguardoID",resguardoID)
                            });
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
                resultado = "ok";
            //}
            //catch { }
            conn.Close();
            
            return new Status { status = resultado };
        }

        public int ObtenerNoResguardo(int banderainventario)
        {

            int bandera = 0;
            string query = "";
            if (banderainventario == 1)
            {
                query = "Select top 1 noresg from Resguardo_Inventario order by ResguardoID DESC";
            }
            else
            {
                query = "Select top 1 noresgcontrol from Resguardo_NoControl order by Resguardo_NoControlID DESC";
            }

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                if (banderainventario == 1)
                {
                    bandera = Convert.ToInt16(ds.Tables[0].Rows[0]["noresg"]);
                }
                else
                {
                    bandera = Convert.ToInt16(ds.Tables[0].Rows[0]["noresgcontrol"]);
                }
            }
            catch
            { }

            if (bandera == 0)
            {
                bandera = 1;
            }
            else
            {
                bandera = bandera + 1;
            }

            return bandera;
        }

        public Resguardo ObtenerResguardoID(int ResguardoID)
        {
            Resguardo r = new Resguardo();
            string query = "SELECT * FROM ResguardoMovil where ResguardoID = " + ResguardoID;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                r.noempleado = Convert.ToInt16(ds.Tables[0].Rows[0]["noempleadoresg"]);
                r.nombreemp = ds.Tables[0].Rows[0]["nombreemp"].ToString();
                r.areadesc = ds.Tables[0].Rows[0]["areadesc"].ToString();
                r.deptodesc = ds.Tables[0].Rows[0]["deptoresg"].ToString();
                r.puestoresg = ds.Tables[0].Rows[0]["puesto"].ToString();
            }
            catch
            {

            }

            return r;
        }

        public int ObtenerNuevo(string noinv)
        {

            int bandera = 0;
            bienID bid = new bienID();
            string query = "SELECT nuevo FROM TrasladodeBienes WHERE inventarionuevotraslado=" + "'" + noinv + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                bandera = Convert.ToInt16(ds.Tables[0].Rows[0]["nuevo"]);

            }
            catch
            {

            }
            return bandera;

        }

        public int ObtenerAltaBienID(string noinv)
        {

            int bandera = 0;
            bienID bid = new bienID();
            string query = "SELECT Alta_BienID FROM Alta_Bien WHERE noinv_nuevo=" + "'" + noinv + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                bandera = Convert.ToInt16(ds.Tables[0].Rows[0]["Alta_BienID"]);

            }
            catch
            {

            }
            return bandera;

        }

        public int RevisarEstatusTransito(string noinv)
        {

            int bandera = 0;
            bienID bid = new bienID();
            string query = "SELECT EstatusTrasladoID FROM TrasladodeBienes WHERE inventarionuevotraslado=" + "'" + noinv + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                bandera = Convert.ToInt16(ds.Tables[0].Rows[0]["EstatusTrasladoID"]);

            }
            catch
            {

            }
            return bandera;

        }

        public Status SetEnviarInventarioNuevo(int IDEdificio, int IDUsuario, string nombreEntrega, string nombreRecibe, string noInv, string noInvViejo, string horaSalida, string Observaciones, string marca, string modelo, string serie, string caracteristicas, int capitalizable, int TipoActivo)
        {

            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;

            try
            {
                DateTime fecharegistro = DateTime.Now;
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO TrasladodeBienes([Usuarioid],[EdificioID],[NomEntrega],[NomRecibe],[inventarionuevotraslado],[inventarioviejotraslado],[fechatraslado],[horatraslado],[EstatusTrasladoID],[observacionestraslado],[nuevo],[marcatraslado],[modelotraslado],[serietraslado],[Caracteristicastraslado],[CapitalizableID],[ACTIVO_ID],[EstatusBienID]) VALUES (@Usuarioid,@EdificioID,@NomEntrega,@NomRecibe,@inventarionuevotraslado,@inventarioviejotraslado,@fechatraslado,@horatraslado,@EstatusTrasladoID,@observacionestraslado,1,@marcatraslado,@modelotraslado,@serietraslado,@Caracteristicastraslado,@CapitalizableID,@ACTIVO_ID,1)";
                    cmd.Parameters.AddRange(new SqlParameter[]
                            {
                                             new SqlParameter("@Usuarioid",IDUsuario),
                                             new SqlParameter("@EdificioID",IDEdificio),
                                             new SqlParameter("@NomEntrega",nombreEntrega),
                                             new SqlParameter("@NomRecibe",nombreRecibe),
                                             new SqlParameter("@inventarionuevotraslado",noInv),
                                             new SqlParameter("@inventarioviejotraslado",noInvViejo),
                                             new SqlParameter("@fechatraslado",fecharegistro),
                                             new SqlParameter("@horatraslado",horaSalida),
                                             new SqlParameter("@EstatusTrasladoID",1),
                                             new SqlParameter("@observacionestraslado",Observaciones),
                                             new SqlParameter("@marcatraslado",marca),
                                             new SqlParameter("@modelotraslado",modelo),
                                             new SqlParameter("@serietraslado",serie),
                                             new SqlParameter("@Caracteristicastraslado",caracteristicas),
                                             new SqlParameter("@CapitalizableID",capitalizable),
                                             new SqlParameter("@ACTIVO_ID",TipoActivo)

                            });
                    cmd.ExecuteNonQuery();
                    resultado = "ok";
                }

            }
            catch
            { }

            return new Status { status = resultado };

        }

        public Status DeleteArticulo(int idarticulo)
        {

            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;

            try
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "DELETE FROM TrasladodeBienes WHERE TrasladodeBienesID = @TrasladodeBienesID";
                    cmd.Parameters.AddRange(new SqlParameter[]
                            {

                                new SqlParameter("@TrasladodeBienesID", idarticulo)

                            });
                    cmd.ExecuteNonQuery();
                    resultado = "ok";
                }

            }
            catch { }
            conn.Close();
            return new Status { status = resultado };

        }


        public bienID BuscarBien(string noInv)
        {

            bienID bien = new bienID();
            string query = "SELECT * FROM Alta_Bien WHERE noinv_nuevo=" + "'" + noInv + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                bien.idbien = Convert.ToInt16(ds.Tables[0].Rows[0]["Alta_BienID"]);
                bien.marca = ds.Tables[0].Rows[0]["marca"].ToString();
                bien.modelo = ds.Tables[0].Rows[0]["modelo"].ToString();
                bien.serie = ds.Tables[0].Rows[0]["serie"].ToString();
                bien.caracteristicas = ds.Tables[0].Rows[0]["caracteristicas"].ToString();
                bien.observaciones = ds.Tables[0].Rows[0]["observaciones"].ToString();
                bien.numinv = ds.Tables[0].Rows[0]["noinv_viejo"].ToString();
                bien.capitalizable = Convert.ToInt16(ds.Tables[0].Rows[0]["capitalizableID"]);
                bien.idactivo = Convert.ToInt16(ds.Tables[0].Rows[0]["ACTIVO_ID"]);
                bien.StatusID = Convert.ToInt16(ds.Tables[0].Rows[0]["EstatusBienID"]);
                bien.EdificioID = ObtenerIDeDIFICIO(bien.idbien);
            }
            catch
            { 

            }
            return bien;

        }



        public int BuscarInventarioTransito(int IDEdificio, string noInv)
        {
            int bandera = 0;
            bienID bid = new bienID();
            string query = "SELECT COUNT(*) as bandera FROM TrasladodeBienes WHERE inventarionuevotraslado=" + "'" + noInv + "' AND EdificioID = " + IDEdificio;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                bandera = Convert.ToInt16(ds.Tables[0].Rows[0]["bandera"]);

            }
            catch
            {

            }
            return bandera;
        }

        public int BuscarInventarioTransitoRecibido(string noInv)
        {

            int bandera = 0;
            bienID bid = new bienID();
            string query = "SELECT COUNT(*) as bandera FROM TrasladodeBienes WHERE inventarionuevotraslado=" + "'" + noInv + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                bandera = Convert.ToInt16(ds.Tables[0].Rows[0]["bandera"]);

            }
            catch
            {

            }
            return bandera;

        }

        public ListaResguardoMovil GetListaResguardoMovil(int noemp)
        {

            ListaResguardoMovil lt = new ListaResguardoMovil();
            lt.Resguardos = new List<Resguardo>();

            String query = "SELECT * FROM ResguardoMovil WHERE noempleadoresg = " + noemp + " ORDER BY fecharesg DESC";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            conn.Close();

            try
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Resguardo r = new Resguardo();
                    r.ResguardoID = Convert.ToInt16(ds.Tables[0].Rows[i]["ResguardoID"]);
                    r.estatusresg = ds.Tables[0].Rows[i]["estastusresg"].ToString();
                    r.noempleado = Convert.ToInt16(ds.Tables[0].Rows[i]["noempleadoresg"]);
                    r.nombreemp = ds.Tables[0].Rows[i]["nombreemp"].ToString();
                    r.areadesc = ds.Tables[0].Rows[i]["areadesc"].ToString();
                    r.deptodesc = ds.Tables[0].Rows[i]["deptoresg"].ToString();
                    lt.Resguardos.Add(r);
                }

            }
            catch
            { }

            return lt;

        }

        public ListaResguardoMovilDetalle GetListaResguardoMovilDetalle(int ResguardoID)
        {

            ListaResguardoMovilDetalle lr = new ListaResguardoMovilDetalle();
            lr.ArticuloResguardos = new List<ArticuloResguardo>();
            String query = "Select ResguardoMovilDetalle.ResguardoDetalleID,ResguardoMovilDetalle.Alta_BienID,Alta_Bien.noinv_nuevo,Alta_Bien.marca,Alta_Bien.modelo,Alta_Bien.serie,Alta_Bien.caracteristicas FROM ResguardoMovilDetalle,Alta_Bien WHERE ResguardoMovilDetalle.Alta_BienID = Alta_Bien.Alta_BienID AND ResguardoID = " + ResguardoID + " ORDER BY fechaestatusbien DESC";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            conn.Close();

            try
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ArticuloResguardo A = new ArticuloResguardo();
                    A.ID = Convert.ToInt16(ds.Tables[0].Rows[i]["ResguardoDetalleID"]);
                    A.BienID = Convert.ToInt16(ds.Tables[0].Rows[i]["Alta_BienID"]);
                    A.noinv = ds.Tables[0].Rows[i]["noinv_nuevo"].ToString();
                    A.marca = ds.Tables[0].Rows[i]["marca"].ToString();
                    A.modelo = ds.Tables[0].Rows[i]["modelo"].ToString();
                    A.serie = ds.Tables[0].Rows[i]["serie"].ToString();
                    A.caracteristicas = ds.Tables[0].Rows[i]["caracteristicas"].ToString();
                    lr.ArticuloResguardos.Add(A);
                }

            }
            catch
            { }

            return lr;
        }

        public Status BorrarResguardoDetalle(int ResguardoDetalleID)
        {

            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;

            try
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "DELETE FROM ResguardoMovilDetalle WHERE ResguardoDetalleID = " + ResguardoDetalleID;
                    cmd.ExecuteNonQuery();
                    resultado = "ok";
                }

            }
            catch { }
            conn.Close();

            return new Status { status = resultado };

        }

        public ListaTransito GetTransito(int IDEdificio)
        {
            
            ListaTransito lt = new ListaTransito();
            lt.bienesTransito = new List<BienTransito>();

            String query = "SELECT * FROM TrasladodeBienes WHERE EdificioID = " + IDEdificio + "AND EstatusTrasladoID = 1 order by fechatraslado DESC";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    BienTransito t = new BienTransito();
                    t.TransitoID = Convert.ToInt16(ds.Tables[0].Rows[i]["TrasladodeBienesID"]);
                    t.NombreEntrega = ds.Tables[0].Rows[i]["NomEntrega"].ToString();
                    t.NombreRecibe = ds.Tables[0].Rows[i]["NomRecibe"].ToString();
                    t.numinv = ds.Tables[0].Rows[i]["inventarionuevotraslado"].ToString();
                    t.numinvViejo = ds.Tables[0].Rows[i]["inventarioviejotraslado"].ToString();
                    t.Observaciones = ds.Tables[0].Rows[i]["observacionestraslado"].ToString();
                    t.nuevo = Convert.ToInt16(ds.Tables[0].Rows[i]["nuevo"]);
                    t.marca = ds.Tables[0].Rows[i]["marcatraslado"].ToString();
                    t.modelo = ds.Tables[0].Rows[i]["modelotraslado"].ToString();
                    t.serie = ds.Tables[0].Rows[i]["serietraslado"].ToString();
                    t.caracteristicas = ds.Tables[0].Rows[i]["Caracteristicastraslado"].ToString();
                    lt.bienesTransito.Add(t);
                }

            }
            catch
            {
                
            }

            return lt;

        }

        public ListaTransito GetTransitoEntregado(int IDEdificio)
        {

            ListaTransito lt = new ListaTransito();
            lt.bienesTransito = new List<BienTransito>();

            String query = "SELECT * FROM TrasladodeBienes WHERE EdificioID = " + IDEdificio + "AND EstatusTrasladoID = 2 order by fechatraslado DESC";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    BienTransito t = new BienTransito();
                    t.TransitoID = Convert.ToInt16(ds.Tables[0].Rows[i]["TrasladodeBienesID"]);
                    t.NombreEntrega = ds.Tables[0].Rows[i]["NomEntrega"].ToString();
                    t.NombreRecibe = ds.Tables[0].Rows[i]["NomRecibe"].ToString();
                    t.numinv = ds.Tables[0].Rows[i]["inventarionuevotraslado"].ToString();
                    t.numinvViejo = ds.Tables[0].Rows[i]["inventarioviejotraslado"].ToString();
                    t.Observaciones = ds.Tables[0].Rows[i]["observacionestraslado"].ToString();
                    t.nuevo = Convert.ToInt16(ds.Tables[0].Rows[i]["nuevo"]);
                    t.marca = ds.Tables[0].Rows[i]["marcatraslado"].ToString();
                    t.modelo = ds.Tables[0].Rows[i]["modelotraslado"].ToString();
                    t.serie = ds.Tables[0].Rows[i]["serietraslado"].ToString();
                    t.caracteristicas = ds.Tables[0].Rows[i]["Caracteristicastraslado"].ToString();
                    lt.bienesTransito.Add(t);
                }
            }
            catch
            {

            }

            return lt;

        }

        public ListaActivo GetActivos()
        {

            ListaActivo la = new ListaActivo();
            la.TipoActivos = new List<TipoActivo>();

            String query = "SELECT * FROM Tipos_Activos";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    TipoActivo t = new TipoActivo();
                    t.TipoID = Convert.ToInt16(ds.Tables[0].Rows[i]["ACTIVO_ID"]);
                    t.Descripcion = ds.Tables[0].Rows[i]["DESCRIPCION_ACTIVO"].ToString();
                    la.TipoActivos.Add(t);
                }
            }
            catch
            {
                
            }

            return la;
        }

        public Status SetEstadoFisico(int IdResgFisico)
        {
            int valor = 0;
            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;

            valor =  VerificarEstado(IdResgFisico);

            if (valor == 1)
            {
                try
                {
                    DateTime fecharegistro = DateTime.Now;
                    conn.Open();
                    using (SqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "Update Resguardo_fisico set Estado = 2, Fecha_Recibido = @Fecha_Recibido WHERE IdResgFisico ='" + IdResgFisico + "'";
                        cmd.Parameters.AddRange(new SqlParameter[]
                                       {
                                             new SqlParameter("@Fecha_Recibido",fecharegistro)
                                       });
                        cmd.ExecuteNonQuery();
                        resultado = "ok";
                    }

                }
                catch { }
                conn.Close();
            }
            else if(valor == 2)
            {
                try
                {
                    DateTime fecharegistro = DateTime.Now;
                    conn.Open();
                    using (SqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "Update Resguardo_fisico set Estado = 1, Fecha_Recibido = @Fecha_Recibido WHERE IdResgFisico ='" + IdResgFisico + "'";
                        cmd.Parameters.AddRange(new SqlParameter[]
                                       {
                                             new SqlParameter("@Fecha_Recibido",fecharegistro)
                                       });
                        cmd.ExecuteNonQuery();
                        resultado = "ok";
                    }

                }
                catch { }
                conn.Close();
            }
            else
            {
                resultado = "duplicado";
            }

            return new Status { status = resultado };
        }

        public Status BorrarInventarioFisico(int IdResgFisico)
        {
            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;

            try
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "DELETE FROM Resguardo_fisico WHERE IdResgFisico ='" + IdResgFisico + "'";
                    cmd.ExecuteNonQuery();
                    resultado = "ok";
                }

            }
            catch { }
            conn.Close();

            return new Status { status = resultado };
        }

        public int VerificarEstado(int IdResgFisico)
        {
            int Estado = 0;
            
            string query = "SELECT * FROM Resguardo_fisico WHERE IdResgFisico ='" + IdResgFisico + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                Estado = Convert.ToInt16(ds.Tables[0].Rows[0]["Estado"]);
            }
            catch
            {

            }

            return Estado;
        }



        public Status UpdateBien(int IDbien, string numinv,string numinvnuevo, string marca, string modelo, string serie, int nofactura, string caracteristicas, string observaciones, int Capitalizable, int EdificioID, int TipoActivo )
        {

            caracteristicas = caracteristicas.Replace("_", " ");
            observaciones = observaciones.Replace("_", " ");

            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            // EDITAR ALTA BIEN
            try
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "UPDATE ALTA_BIEN SET noinv_nuevo = @noinv_nuevo, noinv_viejo = @noinv_viejo, ACTIVO_ID = @ACTIVO_ID, marca = @marca, modelo = @modelo, serie = @serie, nofactura = @nofactura, caracteristicas = @caracteristicas, observaciones = @observaciones, CapitalizableID = @CapitalizableID  WHERE Alta_BienID = @Alta_BienID";
                    cmd.Parameters.AddRange(new SqlParameter[]
                            {

                                new SqlParameter("@noinv_nuevo", numinvnuevo),
                                new SqlParameter("@noinv_viejo", numinv),
                                new SqlParameter("@ACTIVO_ID", TipoActivo),
                                new SqlParameter("@marca", marca),
                                new SqlParameter("@modelo", modelo),
                                new SqlParameter("@serie", serie),
                                new SqlParameter("@nofactura", nofactura),
                                new SqlParameter("@caracteristicas", caracteristicas),
                                new SqlParameter("@observaciones", observaciones),
                                new SqlParameter("@CapitalizableID",Capitalizable),
                                new SqlParameter("@Alta_BienID", IDbien)

                            });
                    cmd.ExecuteNonQuery();
                   // resultado = "ok";
                }

            }
            catch { }
            conn.Close();

            // EDITAR ALTA BIEN DETALLE

            try
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "UPDATE Alta_Bien_Detalle SET EdificioID = @EdificioID WHERE Alta_BienID = @Alta_BienID";
                    cmd.Parameters.AddRange(new SqlParameter[]
                            {
                                new SqlParameter("@EdificioID",EdificioID),
                                new SqlParameter("@Alta_BienID", IDbien)
                            });
                    cmd.ExecuteNonQuery();
                    resultado = "ok";
                }

            }
            catch { }
            conn.Close();

            return new Status { status = resultado };

        }



        public Status DeleteBien(int IDbien)
        {
            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;

            try
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "DELETE FROM ALTA_BIEN WHERE Alta_BienID = @Alta_BienID";
                    cmd.Parameters.AddRange(new SqlParameter[]
                            {

                                new SqlParameter("@Alta_BienID", IDbien)

                            });
                    cmd.ExecuteNonQuery();
                    resultado = "ok";
                }

            }
            catch { }
            conn.Close();
            return new Status { status = resultado };

        }

        #endregion


        #region Edificio

        //             ***Edificio***                    //


        public Edificiom GetEdificiom()
        {

            Edificiom edim = new Edificiom();
            edim.edificiom = new List<Edificio>();


            String query = "SELECT * FROM Edificio";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    Edificio e = new Edificio();
                    e.idedificio = Convert.ToInt16(ds.Tables[0].Rows[i]["EdificioID"]);
                    e.nombre = ds.Tables[0].Rows[i]["descipcionedificion"].ToString();
                    edim.edificiom.Add(e);


                }
            }
            catch { }

            return edim;

        }


        #endregion


        #region Ruleta

        public Status setUsuarioRuleta(string usuario, string clave, string correo, int nivel)
        {

            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionRuleta;

            DateTime time = DateTime.Now;              // Use current time
            string format = "yyyy-MM-dd HH:mm:ss";

            try
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO Usuarios([Usuario],[Clave],[Correo],[nivel],[dtfecha]) VALUES (@usuario,@clave,@correo,@nivel,@dtfecha)";
                    cmd.Parameters.AddRange(new SqlParameter[]
                            {

                             new SqlParameter("@usuario",usuario),
                             new SqlParameter("@clave",clave),
                             new SqlParameter("@correo",correo),
                             new SqlParameter("@nivel",nivel),
                             new SqlParameter("@dtfecha",time.ToString(format))

                            });
                    cmd.ExecuteNonQuery();
                    resultado = "ok";
                }

            }
            catch { }
            conn.Close();
            return new Status { status = resultado };

        }

        public Status setPuntosGanador(string usuario, string clave, int validas)
        {
            string resultado = "Error";
            UsuariosRuletaID usuarioid = new UsuariosRuletaID();
            string query = "SELECT * FROM Ganadores WHERE Usuario='" + usuario + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionRuleta;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    usuarioid.usuario = ds.Tables[0].Rows[0]["Usuario"].ToString();
                }
            }
            catch
            { }
            conn.Close();

            if (usuarioid.usuario == null)
            {
                resultado = setUsuarioGanador(usuario, clave, validas);
            }
            else {
                resultado = UpdateGanador(usuario, clave, validas);
            }

            return new Status { status = resultado };
        }

        public string setUsuarioGanador(string usuario, string clave, int validas)
        {
            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionRuleta;
            
            try
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO Ganadores([Usuario],[Clave],[Validas]) VALUES (@usuario,@clave,@validas)";
                    cmd.Parameters.AddRange(new SqlParameter[]
                            {

                                new SqlParameter("@usuario",usuario),
                                new SqlParameter("@clave",clave),
                                new SqlParameter("@validas",validas)

                            });
                    cmd.ExecuteNonQuery();
                    resultado = "ok";
                }

            }
            catch { }
            conn.Close();
            return resultado;
        }

        public string UpdateGanador(string usuario, string clave, int validas)
        {

            string resultado = "Error";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionRuleta;

            try
            {

                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "UPDATE Ganadores SET validas = @validas WHERE Usuario = @usuario AND Clave = @clave";
                    cmd.Parameters.AddRange(new SqlParameter[]
                            {

                                new SqlParameter("@usuario",usuario),
                                new SqlParameter("@clave",clave),
                                new SqlParameter("@validas",validas)

                            });
                    cmd.ExecuteNonQuery();
                    resultado = "ok";
                }

            }
            catch { }
            conn.Close();
            return resultado;

        }


        public GanadorID GetGanadorID(String usuario)
        {

            GanadorID ganadorID = new GanadorID();
            string query = "SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY validas DESC) AS Rank, *FROM Ganadores) Ganadores WHERE Usuario = '"+usuario+"'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionRuleta;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

            try
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    
                    ganadorID.rank = Convert.ToInt16(ds.Tables[0].Rows[0]["Rank"]);
                    ganadorID.usuario = ds.Tables[0].Rows[0]["Usuario"].ToString();
                    ganadorID.clave = ds.Tables[0].Rows[0]["Clave"].ToString();
                    ganadorID.puntos = Convert.ToInt16(ds.Tables[0].Rows[0]["validas"]);

                }

            }
            catch { }

            conn.Close();
            return ganadorID;

        }

        public UsuariosRuletaID getloginRuleta(string usuario, string clave)
        {

            UsuariosRuletaID usuarioid = new UsuariosRuletaID();
            string query = "SELECT * FROM Usuarios WHERE Usuario='" + usuario + "' AND Clave='" + clave + "' ";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionRuleta;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    usuarioid.usuario = ds.Tables[0].Rows[0]["Usuario"].ToString();
                    usuarioid.clave = ds.Tables[0].Rows[0]["Clave"].ToString();
                    usuarioid.correo = ds.Tables[0].Rows[0]["Correo"].ToString();
                    usuarioid.nivel = Convert.ToInt16(ds.Tables[0].Rows[0]["nivel"]);


                }
            }
            catch
            { }

            conn.Close();
            return usuarioid;

        }

        public preguntaarray GetPreguntas()
        {

            preguntaarray preguntarray = new preguntaarray();
            preguntarray.preguntas = new List<Pregunta>();      

            string query = "SELECT Preguntas.pregunta AS pregunta,Preguntas.Descripcion AS Descripcion,Preguntas.categoria AS categoria FROM Preguntas";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionRuleta;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    Pregunta p = new Pregunta();

                    Pregunta pregunta = new Pregunta();
                    pregunta.respuestas = new List<Respuesta>();

                    p.ID = Convert.ToInt16(ds.Tables[0].Rows[i]["pregunta"]);
                    p.descripcion = ds.Tables[0].Rows[i]["Descripcion"].ToString();
                    p.categoriaID = Convert.ToInt16(ds.Tables[0].Rows[i]["categoria"]);

                    string query2 = "SELECT Respuestas.respuesta AS respuesta,Respuestas.Descripcion AS Descripcion,Preguntas_Respuestas.valida AS valida,Preguntas_Respuestas.pregunta AS pregunta FROM Respuestas,Preguntas_Respuestas WHERE Respuestas.respuesta = Preguntas_Respuestas.respuesta AND Preguntas_Respuestas.pregunta = " + p.ID ;
                    SqlConnection conn2 = new SqlConnection();
                    conn2.ConnectionString = ConexionRuleta;
                    conn2.Open();

                    DataSet ds2 = new DataSet();
                    SqlCommand cmd2 = new SqlCommand(query2, conn2);
                    SqlDataAdapter adapter2 = new SqlDataAdapter(cmd2);
                    adapter2.Fill(ds2);
                    conn2.Close();

                    for (int x = 0; x < ds2.Tables[0].Rows.Count; x++)
                    {

                        Respuesta r = new Respuesta();
                        r.ID = Convert.ToInt16(ds2.Tables[0].Rows[x]["respuesta"]);
                        r.Descripcion = ds2.Tables[0].Rows[x]["Descripcion"].ToString();
                        r.valida = Convert.ToInt16(ds2.Tables[0].Rows[x]["valida"]);
                        r.preguntaID = Convert.ToInt16(ds2.Tables[0].Rows[x]["pregunta"]);
                        pregunta.respuestas.Add(r);

                    }

                    p.respuestas = pregunta.respuestas;
                    preguntarray.preguntas.Add(p);

                }

                

            }
            catch { }
            return preguntarray;

        }

        public Categorias GetCategorias()
        {
            Categorias categorias = new Categorias();
            categorias.categorias = new List<Categoria>();


            string query = "SELECT * From Categorias";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionRuleta;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    Categoria c = new Categoria();
                    c.ID = Convert.ToInt16(ds.Tables[0].Rows[i]["categoria"]);
                    c.nombre = ds.Tables[0].Rows[i]["Descripcion"].ToString();
                    categorias.categorias.Add(c);

                }
            }
            catch { }

            return categorias;

        }


        public Ganadores GetGanadores()
        {

            Ganadores ganadores = new Ganadores();
            ganadores.ganadores = new List<Ganador>();

            string query = "SELECT TOP 5 * From Ganadores Order BY validas DESC";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionRuleta;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            try
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    Ganador g = new Ganador();
                    g.usuario = ds.Tables[0].Rows[i]["Usuario"].ToString();
                    g.clave = ds.Tables[0].Rows[i]["Clave"].ToString();
                    g.puntos = Convert.ToInt16(ds.Tables[0].Rows[i]["validas"]);
                    ganadores.ganadores.Add(g);

                }

            }
            catch { }

            return ganadores;

        }

        public Status EnviarCorreo(string correo)
        {
            string resultado = "Error";
            string Usuario = null;
            string contraseña = null;
            string query = "SELECT * FROM Usuarios WHERE Correo='" + correo + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConexionRuleta;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Usuario = ds.Tables[0].Rows[0]["Usuario"].ToString();
                    contraseña = ds.Tables[0].Rows[0]["Clave"].ToString();
                }
            }
            catch
            { }
            conn.Close();

            if (Usuario == null)
            {
                resultado = "NoExiste";
            }
            else {
                try
                {
                    SmtpClient smtpClient = new SmtpClient("mail.ieebc.mx", 587);
                    smtpClient.Credentials = new System.Net.NetworkCredential("participacionciudadana@ieebc.mx", "participa2017");
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.EnableSsl = true;
                    MailMessage mail = new MailMessage();

                    //Setting From , To and CC
                    mail.From = new MailAddress("participacionciudadana@ieebc.mx", "COORDINACIÓN DE PARTICIPACIÓN CIUDADANA");
                    mail.To.Add(new MailAddress(correo));
                    mail.Subject = "RECUPERACIÓN DE CONTRASEÑA KRATOS Y LA RULETA DEL SABER";
                    mail.Body = "Haz solicitado la recuperación de tu contraseña, esta es la siguiente: " + contraseña;
                    smtpClient.Send(mail);
                    resultado = "ok";
                }
                catch (Exception ex)
                { 
                }
            }

            return new Status { status = resultado };
        }

        #endregion

    }

}


